Version 0.7.1
=============
Released: 2020-02-02

 * Fixed GST not starting with 2 columns for some users

Version 0.7.0
=============
Released: 2020-02-02

 * Added emoji to sensor feature types (⚡, ️🌡, 💧,...)
 * Improved core usage indicator creation algorithm
 * Handling sensor chip subfeature not being readable
 * HWMON: displaying average value as current if current is not available
 * Added clarification text on how to read RAM info
 * Added check to verify if `dmidecode` is available on the host
 * Memory dropdown: pre-selecting the first installed module of RAM
 * Fix #5: Window doesn't fit on 1366x768

Version 0.6.1
=============
Released: 2020-01-26

 * Fixed error when OEM specific DMI type is present on the `dmidecode` output
 * Fixed Hardware monitor values having up to 15 decimals for some users
 * Fixed Core usage LevelBar changing colors due to default offsets

Version 0.6.0
=============
Released: 2020-01-26

 * Name changed to GtkStressTesting

Version 0.5.0
=============
Released: 2020-01-25

 * Initial release
